/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorredes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario1
 */
public class GestionEsclavo implements Runnable{
    private Socket servidorMaestro;
    private final String rutaDirectorioServidor = "/Users/Usuario1/proyecto-de-redes-ii-201322/ServidorRedes/Directorio/Esclavo/";
    Thread hilo;
    
    
    public GestionEsclavo(Socket socket) throws UnknownHostException, IOException {
        this.servidorMaestro = socket;
        this.hilo = new Thread(this);
        this.hilo.start();
    }
    
    private Object recibirObjeto(){
        ObjectInputStream mensajeServidor;
        try {
            mensajeServidor = new ObjectInputStream(servidorMaestro.getInputStream());
            Object mensaje = mensajeServidor.readObject();
            return mensaje;
        }
        catch (ClassNotFoundException | IOException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }   
        return null;
    }
    
     private void recibirArchivo(String nombreArchivo){
         
         try{
            File archivoRecibido = archivoRecibido = (File)recibirObjeto(); // recibo el FILE del servidor
            FileInputStream fis = new FileInputStream(archivoRecibido);
            byte[] buffer = new byte[(int) archivoRecibido.length() ];

           // System.out.println("Recibiendo Archivo: "+nombreArchivo);
            fis.read(buffer); // paso el archivo a byte
            
            FileOutputStream rutaArchivo = new FileOutputStream(rutaDirectorioServidor+nombreArchivo);
            rutaArchivo.write(buffer);

            System.out.println("Archivo Recibido");
        } catch (FileNotFoundException ex) {
            System.out.println("El archivo "+nombreArchivo+ " NO se logro crear en el directorio");
        }catch (IOException ex) {
            System.out.println("El archivo NO se pudo Recibir");
        }catch (NullPointerException e){
            System.out.println("El archivo "+nombreArchivo+ " NO existe en el servidor");
        }
     }
     
     
    @Override
    public void run() {
        try {
                String mensaje = (String) recibirObjeto();
                String[] comando = mensaje.split(" ");
                Log log = new Log("/Users/Usuario1/proyecto-de-redes-ii-201322/ServidorRedes/Directorio/Esclavo/bitacora.txt");

                    switch(comando[0]){

                        case "put":
                            recibirArchivo(comando[1]);
                            log.escribir(mensaje, "realizado");
                        break;

                    }
        }
        catch (IOException ex) {
            Logger.getLogger(GestionEsclavo.class.getName()).log(Level.SEVERE, null, ex);
        }  
        
    }
    
}
 