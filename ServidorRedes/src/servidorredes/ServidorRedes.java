/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorredes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;

/**
 *
 * @author Isaac Arismendi
 */
public class ServidorRedes {

    private static Conexion servidor;
    public static void main(String[] args) throws IOException { 
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String consola = "";
        servidor = null;
        try{
             servidor = new Conexion();
        }
        catch(NumberFormatException e){
               System.out.println(e);
           }
        catch(NullPointerException e){
            System.out.println("Error NO se puede iniciar el servidor");
        }
        while(!consola.equals("exit")){
            System.out.print(servidor.getTipoServidor()+"> ");
            consola = br.readLine();
            switch(consola){
                
                case "exit":
                    System.out.println("Hasta luego!");
                    servidor.exit();
                break;
                    
                case "info":
                    InetAddress thisIp = InetAddress.getLocalHost();
                    System.out.println("Informacion del servidor "+servidor.getTipoServidor());
                    String x = servidor.getServidor().getInetAddress().toString();
                    System.out.println("Host name: "+thisIp.getHostName());
                    System.out.println("ip: "+thisIp.getHostAddress());
                    System.out.println("puerto Nº: "+servidor.getServidor().getLocalPort());
                break;
                    
            }       
        } 
        
    }
    
    public static void cambiarEsclavoAMaestro() throws IOException{
        servidor.servidorMaestro();
    }
}
