/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorredes;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Usuario1
 */
public class Log {
    private  FileWriter fichero = null;
    private PrintWriter pw = null;
    private String ruta;
    
    public Log(String ruta) throws IOException{
        this.ruta = ruta;
    }
    
    public void escribir(String registo, String status) throws IOException{
        this.fichero = new FileWriter(this.ruta, true);
        this.pw = new PrintWriter(this.fichero);
        DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy:HH:mm:ss");
        Date fecha = new Date(); 
         InetAddress thisIp = InetAddress.getLocalHost();
        pw.println(formatoFecha.format(fecha) + ":" +thisIp.getHostAddress() + 
                    ":" + registo + ":" + status);
        this.fichero.close();
    }
}
