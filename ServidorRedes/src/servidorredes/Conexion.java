/* s
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorredes;

import java.io.IOException;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 *
 * @author Isaac Arismendi
 */
public class Conexion implements Runnable{
    
    private ServerSocket servidor;
    private int puerto;
    private String tipoServidor;
    private Thread hilo;

    public Conexion() throws IOException {
        
        try{
            servidorMaestro();
        } 
        catch (BindException e) {
            try{
                servidorEsclavo();
            }
            catch(BindException ex){
             System.out.println("Los dos servidores estan en funcionamiento");
             }
        } 
        
    }
    
    public void servidorMaestro() throws IOException{
        this.puerto = 7771;
        this.servidor = new ServerSocket(this.puerto);
        this.tipoServidor = "Maestro";
        System.out.println("Servidor "+this.tipoServidor+ " encendido");
        hilo = new Thread(this);
        hilo.start();
    }
    
    public  void servidorEsclavo() throws UnknownHostException, IOException{        
        new MaestroActivo(this);
        this.puerto = 7772;
        this.servidor = new ServerSocket(this.puerto);
        this.tipoServidor = "Esclavo";
        System.out.println("Servidor "+this.tipoServidor+ " encendido");
        hilo = new Thread(this);
        hilo.start();
        
    }
    
    public void exitServidor() throws IOException {
        this.servidor.close();
    }
    
    public void exit() throws IOException{
        this.servidor.close();
        System.exit(0);
    }
    
    public String getTipoServidor() {
        return tipoServidor;
    }

    public void setTipoServidor(String tipoServidor) {
        this.tipoServidor = tipoServidor;
    }
    
    public ServerSocket getServidor() {
        return servidor;
    }

    public void setServidor(ServerSocket servidor) {
        this.servidor = servidor;
    }
    
    @Override
    public void run() {
        while (true) {
            try{
                if(tipoServidor.equals("Maestro")){
                    Socket cliente = servidor.accept();
                    new GestionServidor(cliente);
                }
                else{
                    Socket servidorMaestro = servidor.accept();
                    new GestionEsclavo(servidorMaestro);
                }                
            }
            catch(IOException e){
                System.out.println("Servidor apagado");
                break;
            }
        } 
    }    
}
