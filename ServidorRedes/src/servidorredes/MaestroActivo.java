/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorredes;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Usuario1
 */
public class MaestroActivo implements Runnable{
    private Socket servidorMaestro;
    private static String SERVIDOR = "localhost";
    private static int NUMERO_PUERTO = 7771;
    private Conexion conexion;
    private Thread hilo;
    
    public MaestroActivo(Conexion conexion) throws UnknownHostException, IOException{
        conexion();
        this.hilo = new Thread(this);
        this.hilo.start();  
        this.conexion = conexion;
    }
    
    private void conexion() throws UnknownHostException, IOException{
        this.servidorMaestro = new Socket(SERVIDOR, NUMERO_PUERTO);
    }
    private void enviarObjeto(Object mensaje) throws IOException{
        ObjectOutputStream enviarMensaje = new ObjectOutputStream(servidorMaestro.getOutputStream());
        enviarMensaje.writeObject(mensaje);
    }
    
    private Object recibirObjeto(){
        ObjectInputStream mensajeServidor;
        try {
            mensajeServidor = new ObjectInputStream(servidorMaestro.getInputStream());
            Object mensaje = mensajeServidor.readObject();
            return mensaje;
        }
        catch (ClassNotFoundException | IOException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }   
        return null;
    }
    
    @Override
    public void run() {
        String ping = "ping";
       while(ping.equals("ping"))
       {          
            try {
                conexion();
                enviarObjeto("ping");
                ping = (String)recibirObjeto();
                Thread.sleep(5000); 
            } 
            catch(ConnectException ex){
               try {
                   this.conexion.exitServidor();    
                   ServidorRedes.cambiarEsclavoAMaestro();
                   ping = "exit";
               } catch (IOException ex1) {
                   Logger.getLogger(MaestroActivo.class.getName()).log(Level.SEVERE, null, ex1);
               }
            }
            catch (InterruptedException ex) {
                Logger.getLogger(MaestroActivo.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch (IOException ex) {
                Logger.getLogger(MaestroActivo.class.getName()).log(Level.SEVERE, null, ex);
                ping = "exit";
            }          
       } 
    }
    
}
