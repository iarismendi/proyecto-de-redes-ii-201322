/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package servidorredes;


import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author  Isaac Arismendi
 */
public class GestionServidor implements Runnable{
    private Socket cliente;
    private File Directorio;
    private final String rutaDirectorioServidor = "/Users/Usuario1/proyecto-de-redes-ii-201322/ServidorRedes/Directorio/";
    private Thread hilo;
    private final String ipServidorEsclavo = "localhost";
    private final int puertoServidorEsclavo = 7772;
    
    public GestionServidor(Socket socket) throws EOFException{
        this.Directorio = new File(rutaDirectorioServidor);
        this.cliente = socket;
        this.hilo = new Thread(this);
        this.hilo.start();
    }
    
    private Object recibirObjeto(Socket cliente){
        ObjectInputStream mensajeServidor;
        try {
            mensajeServidor = new ObjectInputStream(cliente.getInputStream());
            Object mensaje = mensajeServidor.readObject();
            return mensaje;
        }
        catch (ClassNotFoundException | IOException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }   
        return null;
    }
    
     private void enviarObjeto(Object mensaje, Socket cliente) throws IOException{
        ObjectOutputStream enviarMensaje = new ObjectOutputStream(cliente.getOutputStream());
        enviarMensaje.writeObject(mensaje);
    }
    
     private void enviarArchivo(String nombreArchivo, Socket cliente) throws IOException {
         
         File archivoEnviar = new File(rutaDirectorioServidor+nombreArchivo);
         if(archivoEnviar.exists()){
            enviarObjeto(archivoEnviar,cliente);
            System.out.println("Archivo enviado: "+nombreArchivo);
         }
         else{
             enviarObjeto(null, cliente);
             System.out.println("EL archivo "+nombreArchivo+" NO"
                     + " en el servidor");
         }     
     }
     
     private void recibirArchivo(String nombreArchivo){
         
        try{
           File archivoRecibido = archivoRecibido = (File)recibirObjeto(this.cliente); // recibo el FILE del servidor
           FileInputStream fis = new FileInputStream(archivoRecibido);
           byte[] buffer = new byte[(int) archivoRecibido.length() ];

           //System.out.println("Recibiendo Archivo: "+nombreArchivo);
           fis.read(buffer); // paso el archivo a byte

           FileOutputStream rutaArchivo = new FileOutputStream(rutaDirectorioServidor+nombreArchivo);
           rutaArchivo.write(buffer);

           System.out.println("Archivo Recibido");
        } catch (FileNotFoundException ex) {
           System.out.println("El archivo "+nombreArchivo+ " NO se logro crear en el directorio");
        }catch (IOException ex) {
           System.out.println("El archivo NO se pudo Recibir");
        }catch (NullPointerException e){
           System.out.println("El archivo "+nombreArchivo+ " NO existe en el servidor");
        }
     }
     
     private void enviarArchivoEsclavo(String mensaje,String nombreArchivo) throws UnknownHostException, IOException{
         try{
            Socket servidorEsclavo = new Socket(ipServidorEsclavo, puertoServidorEsclavo);
            enviarObjeto(mensaje, servidorEsclavo);
            enviarArchivo(nombreArchivo, servidorEsclavo);
         }
         catch(ConnectException ex){
             System.out.println("Servidor esclavo NO conectado");
         }
         
     }
     
    @Override
    public void run(){
        ObjectInputStream mensajeCliente;
        try {
            Log log = new Log("/Users/Usuario1/proyecto-de-redes-ii-201322/ServidorRedes/Directorio/bitacora.txt");
            mensajeCliente = new ObjectInputStream(cliente.getInputStream());
            String mensaje = (String) mensajeCliente.readObject();
            String[] comando = mensaje.split(" ");
            switch(comando[0]){
                
                case "ls":
                    String[] ficheros = Directorio.list();
                    enviarObjeto(ficheros, this.cliente);
                    log.escribir(mensaje, "realizado");
                break;
                    
                case "get":
                    enviarArchivo(comando[1], this.cliente); 
                    log.escribir(mensaje, "realizado");
                break;
                    
                case "put":
                    recibirArchivo(comando[1]);  
                    enviarArchivoEsclavo(mensaje, comando[1]);
                    log.escribir(mensaje, "realizado");
                break;
                    
                case "ping":
                    enviarObjeto("ping",this.cliente);
                break;
                                   
                default:
                    enviarObjeto("Comando NO reconocido", this.cliente);
                    log.escribir(mensaje, "Comando NO reconocido");
                break;         
            }
        }
        catch(FileNotFoundException ex){
            System.out.println("El archivo log no se puedo cargar");
        }
        catch(EOFException ex){
            System.out.println("Cliente desconectado");
        }
        catch (ClassNotFoundException | IOException ex) {
            Logger.getLogger(GestionServidor.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
   
}
