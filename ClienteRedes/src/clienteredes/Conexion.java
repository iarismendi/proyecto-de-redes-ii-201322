/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteredes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author  Isaac Arismendi
 */
public class Conexion {
    private static String ipServidor = "localhost";

    private static int NUMERO_PUERTO = 7771;
    private Socket servidor;
    private String rutaDirectorioCliente = "/Users/Usuario1/proyecto-de-redes-ii-201322/ClienteRedes/Directorio/";
    
    public Conexion() throws UnknownHostException, IOException{
        this.servidor = new Socket(ipServidor, NUMERO_PUERTO);
    }
    
     public String getSERVIDOR() {
        return ipServidor;
    }

    public void setSERVIDOR(String SERVIDOR) {
        Conexion.ipServidor = SERVIDOR;
    }
    
    private void enviarObjeto(Object mensaje) throws IOException{
        ObjectOutputStream enviarMensaje = new ObjectOutputStream(servidor.getOutputStream());
        enviarMensaje.writeObject(mensaje);
    }
    
    private Object recibirObjeto(){
        ObjectInputStream mensajeServidor;
        try {
            mensajeServidor = new ObjectInputStream(servidor.getInputStream());
            Object mensaje = mensajeServidor.readObject();
            return mensaje;
        }
        catch (ClassNotFoundException | IOException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }   
        return null;
    }

    
    public void ls() throws IOException{
        enviarObjeto("ls");
        String[] mensaje = (String[]) recibirObjeto();
        System.out.println("Lista de archivos del servidor:");
        for (String string : mensaje) {
            System.out.println(string);
        }
    }
    
    public void get(String comando, String nombreArchivo){
        try {
            enviarObjeto(comando);

            File archivoRecibido = archivoRecibido = (File)recibirObjeto(); // recibo el FILE del servidor
            FileInputStream fis = new FileInputStream(archivoRecibido);
            byte[] buffer = new byte[(int) archivoRecibido.length() ];

            System.out.println("Recibiendo Archivo: "+nombreArchivo);
            fis.read(buffer); // paso el archivo a byte
            
            FileOutputStream rutaArchivo = new FileOutputStream(rutaDirectorioCliente+nombreArchivo);
            rutaArchivo.write(buffer);

            System.out.println("Archivo Recibido");
        } catch (FileNotFoundException ex) {
            System.out.println("El archivo "+nombreArchivo+ " NO se logro crear en el directorio");
        }catch (IOException ex) {
            System.out.println("El archivo NO se pudo Recibir");
        }catch (NullPointerException e){
            System.out.println("El archivo "+nombreArchivo+ " NO existe en el servidor");
        }
        
    }
    
    public void lls(){
        File carpetaLocal = new File(rutaDirectorioCliente);
        String[] archivos = carpetaLocal.list();
        System.out.println("Lista de archivos local:");
        for (String string : archivos) {
            System.out.println(string);
        }
    }
    
    public void put(String comando, String nombreArchivo) throws IOException{
        enviarObjeto(comando);
        File archivoEnviar = new File(rutaDirectorioCliente+nombreArchivo);
         if(archivoEnviar.exists()){
            enviarObjeto(archivoEnviar);
            System.out.println("Archivo enviado: "+nombreArchivo);
         }
         else{
             enviarObjeto(null);
             System.out.println("EL archivo "+nombreArchivo+" NO"
                     + " se encuntra en el directorio");
         }    
        
    }
    
    
}
