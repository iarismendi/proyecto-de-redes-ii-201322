/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package clienteredes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;

/**
 *
 * @author Isaac Arismendi
 */
public class ClienteRedes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnknownHostException, IOException {
      
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String consola = "";
        while(!consola.equals("exit")){
             try{
                Conexion servidor = new Conexion();
                System.out.print("Cliente> ");
                consola = br.readLine();
                String[] comando = consola.split(" ");
           
                
                switch(comando[0]){
                    
                    case "server":
                        servidor.setSERVIDOR(comando[1]);
                    break;
                    
                    case "ls":
                        servidor.ls();
                    break;
                        
                    case "get":
                        servidor.get(consola, comando[1]);
                    break;
                        
                    case "exit":
                        System.out.println("Hasta luego!");
                        System.exit(0);
                    break;
                        
                    case "lls":
                        servidor.lls();
                    break;
                        
                    case "put":
                        servidor.put(consola, comando[1]);
                    break;
                        
                    default:
                        System.out.println("Comando NO reconocido");
                    break;
                }
                        
            }
            catch(IOException e){
                System.out.println("No se encontro el servidor"); 
                System.exit(0);
             }
        }
        
        // TODO code application logic here
    }
}
